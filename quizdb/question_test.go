package quizdb

import "fmt"

func ExampleValidMCQuestion() {
	q1 := MakeQuestion(
		"What is a cat?", 2,
		"A mouse.", "A vicious predator.", "A cuddly pet.", "Something else.",
	)

	fmt.Println(q1.IsValid())
	fmt.Println(q1.IsMultipleChoice())
	fmt.Println(q1.IsOpen())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	// Output:
	// true
	// true
	// false
	// What is a cat?
	// 4
	// A cuddly pet.
}

func ExampleInvalidMCQuestion() {
	q1 := MakeQuestion(
		"What is a cat?", 5,
		"A mouse.", "A vicious predator.", "A cuddly pet.", "Something else.",
	)

	fmt.Println(q1.IsValid())
	fmt.Println(q1.IsMultipleChoice())
	fmt.Println(q1.IsOpen())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	// Output:
	// false
	// false
	// false
	//
	// -1
	//
}

func ExampleValidOpenQuestion() {
	q1 := MakeQuestion(
		"What is a cat?", 0,
		"My favourite animal.",
	)

	fmt.Println(q1.IsValid())
	fmt.Println(q1.IsMultipleChoice())
	fmt.Println(q1.IsOpen())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	// Output:
	// true
	// false
	// true
	// What is a cat?
	// 1
	// My favourite animal.
}

func ExampleInvalidOpenQuestions() {
	q1 := MakeQuestion(
		"What is a cat?", 1,
		"My favourite animal.",
	)
	q2 := MakeQuestion(
		"What is a doc?", 0,
	)

	fmt.Println(q1.IsValid())
	fmt.Println(q1.IsMultipleChoice())
	fmt.Println(q1.IsOpen())
	fmt.Println(q1.QuestionText())
	fmt.Println(q1.ChoiceCount())
	fmt.Println(q1.CorrectAnswer())

	fmt.Println(q2.IsValid())
	fmt.Println(q2.IsMultipleChoice())
	fmt.Println(q2.IsOpen())
	fmt.Println(q2.QuestionText())
	fmt.Println(q2.ChoiceCount())
	fmt.Println(q2.CorrectAnswer())

	// Output:
	// false
	// false
	// false
	//
	// -1
	//
	// false
	// false
	// false
	//
	// -1
	//
}

func ExampleReadValidMCQuestion() {
	q1 := MakeQuestion(
		"What is a cat?", 2,
		"A mouse.", "A vicious predator.", "A cuddly pet.", "Something else.",
	)

	fmt.Println(q1.Read())

	// Output:
	// What is a cat?
	//     A: A mouse.
	//     B: A vicious predator.
	//  -> C: A cuddly pet.
	//     D: Something else.
}
