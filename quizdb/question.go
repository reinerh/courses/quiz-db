package quizdb

import (
	"fmt"
	"strings"
)

// A question with a list of answers and
// specification of correct answer.
type Question struct {
	questionText  string
	answerChoices []string
	correctAnswer int
}

// Checks if a Question ist valid.
// A question ist valid iff there is at least one answer choice and the correct answer
// number is given as one of the choices.
func (q Question) IsValid() bool {
	return q.correctAnswer >= 0 && len(q.answerChoices) > q.correctAnswer
	// Remark: q.ChoiceCount() cannot be used as this would lead to a loop.
}

// Checks if the question is a multiple choice question.
func (q Question) IsMultipleChoice() bool {
	return q.IsValid() && q.ChoiceCount() > 1
}

// Checks if the question is an open question.
func (q Question) IsOpen() bool {
	return q.IsValid() && q.ChoiceCount() == 1
}

// Returns the question text if the question is valid.
// Otherwise returns the empty string.
func (q Question) QuestionText() string {
	if !q.IsValid() {
		return ""
	}
	return q.questionText
}

// Returns the number of answer choices for the question if the question ist valid.
// Otherwise returns -1.
func (q Question) ChoiceCount() int {
	if !q.IsValid() {
		return -1
	}
	return len(q.answerChoices)
}

// Returns the correct answer if the question is valid.
// Otherwise returns the empty string.
func (q Question) CorrectAnswer() string {
	if !q.IsValid() {
		return ""
	}
	return q.answerChoices[q.correctAnswer]
}

// Returns a string that can be read to quiz participants.
// Returns the empty string for invalid questions.
// The string contains a marker that shows the correct answer.
func (q Question) Read() string {
	if !q.IsValid() {
		return ""
	}

	firstLetter := int('A')
	result := []string{q.QuestionText()}
	for i, choice := range q.answerChoices {
		marker := func() string {
			if i == q.correctAnswer {
				return "->"
			} else {
				return "  "
			}
		}
		result = append(result, fmt.Sprintf(" %s %c: %s", marker(), firstLetter+i, choice))
	}
	result = append(result, "")

	return strings.Join(result, "\n")
}

// Creates a new question.
func MakeQuestion(questionText string, correctAnswer int, answerChoices ...string) Question {
	return Question{questionText, answerChoices, correctAnswer}
}
