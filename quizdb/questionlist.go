package quizdb

type QuestionWithID struct {
	id int
	Question
}
type QuestionList []QuestionWithID

func MakeQuestionList(questions ...Question) QuestionList {
	result := make(QuestionList, 0)
	for id, question := range questions {
		result = append(result, QuestionWithID{id, question})
	}
	return result
}
