package main

import (
	"fmt"

	"gitlab.com/reinerh/courses/quiz-db/quizdb"
)

func main() {

	q1 := quizdb.MakeQuestion(
		"What is a cat?", 2,
		"A mouse.", "A vicious predator.", "A cuddly pet.", "Something else.",
	)
	q2 := quizdb.MakeQuestion(
		"What is a dog?", 0,
		"Someone's best friend.",
	)
	fmt.Println(q1)
	fmt.Println(q1.Read())
	fmt.Println(q2)
	fmt.Println(q2.Read())

	l1 := quizdb.MakeQuestionList(q1, q2)

	fmt.Println(l1)
	fmt.Printf("%T\n", l1[0])
	fmt.Println(l1[0].Read())
	fmt.Println(l1[1].Read())

}
